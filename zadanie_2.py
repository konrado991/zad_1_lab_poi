import numpy as np
import pyransac3d as py
import random
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
import csv
import matplotlib.pyplot as plt

#Import pliku csv

def importowanie_chmury_puntow():
    with open('cloud_joined.xyz', 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for x, y, z in reader:
            yield x, y, z

chmura_punktow = []
for line in importowanie_chmury_puntow():
    chmura_punktow.append(line)

def importowanie_poziomo():
    with open('poziomo.xyz', 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for x, y, z in reader:
            yield x, y, z

poziomo = []
for line in importowanie_poziomo():
    poziomo.append(line)

def importowanie_pionowo():
    with open('pionowo.xyz', 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for x, y, z in reader:
            yield x, y, z

pionowo = []
for line in importowanie_pionowo():
    pionowo.append(line)

def importowanie_cylinder():
    with open('cylinder.xyz', 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for x, y, z in reader:
            yield x, y, z

cylinder = []
for line in importowanie_cylinder():
    cylinder.append(line)

#Algorytm K-srednich

def K_srednich(zbior_punktow):
    clusterer = KMeans(n_clusters=3)

    X_clus = np.array(zbior_punktow, dtype=float)
    y_pred = clusterer.fit_predict(X_clus)

    red = y_pred == 0
    blue = y_pred == 1
    cyan = y_pred == 2

    wykres = plt.figure(figsize =(10,10))
    os = wykres.add_subplot(projection='3d')

    os.scatter(X_clus[red, 0], X_clus[red, 1], X_clus[red, 2], c="red")
    os.scatter(X_clus[blue, 0], X_clus[blue, 1], X_clus[blue, 2], c="blue")
    os.scatter(X_clus[cyan, 0], X_clus[cyan, 1], X_clus[cyan, 2], c="cyan")
    plt.title('Algorytm K-srednich', fontsize=12)
    os.set_xlabel('x', fontsize=12)
    os.set_ylabel('y', fontsize=12)
    os.set_zlabel('z', fontsize=12)
    plt.tight_layout()
    plt.show()

K_srednich(chmura_punktow)

#Algoytm Ransac

def ransac(zbior_punktow, odchylka):
    X = np.array(zbior_punktow, dtype=float)
    A = random.choice(X)
    B = random.choice(X)
    C = random.choice(X)

    wektorA = np.subtract(A, C)
    wektorB = np.subtract(B, C)

    Ua = wektorA / np.linalg.norm(wektorA)
    Ub = wektorB / np.linalg.norm(wektorB)

    n = np.cross(Ua, Ub)

    d = -np.sum(np.multiply(C, n))

    odl_punktow = (n[0] * X[:, 0] + n[1] * X[:, 1] + n[2] * X[:, 2]) + d

    inliers = np.where(np.abs(odl_punktow) <= odchylka)[0]

    print('Wspolczynniki plaszczyzny: ', '\na: ', n[0], '\nb: ', n[1], '\nc: ', n[2], '\nd: ', d,
          '\n\nNumber of inliers equals to: ', inliers)

    if n[1] == 0 and n[0] == 0 and n[2] != 0:
        print('Plaszczyzna jest pozioma', '\n')
    elif (n[0] != 0 or n[1] != 0) and n[2] == 0:
        print('Plaszczyzna jest pionowa', '\n')
    else:
        print('Plaszczyzna nie jest pozioma oraz nie jest pionowa', '\n')

#sprawdzenie dla ransac

ransac(poziomo, 0.5)
ransac(pionowo, 0.5)
#ransac(cylinder, 0.5)
ransac(chmura_punktow, 0.1)

#Algoytm Ransac ransac3d_package
def ransac3d_plane(zbior_punktow):
    X = np.array(zbior_punktow, dtype=float)
    plaszczyzna = py.Plane()
    best_eq, best_inliers = plaszczyzna.fit(X, 0.01)
    print('\n\nWspolczynniki plaszczyzny:', '\na: ', best_eq[0], '\nb: ', best_eq[1], '\nc: ', best_eq[2],
          '\nd: ', best_eq[3], '\n')

    if best_eq[1] == 0 and best_eq[0] == 0 and best_eq[2] != 0:
        print('Plaszczyzna jest pozioma', '\n')
    elif (best_eq[0] != 0 or best_eq[1] != 0) and best_eq[2] == 0:
        print('Plaszczyzna jest pionowa', '\n')
    else:
        print('Plaszczyzna nie jest pozioma oraz nie jest pionowa', '\n')

ransac3d_plane(chmura_punktow)

#Algorytm DBSCAN

def Alg_DBSCAN(zbior_punktow, eps, min_probek):
    X_clus = np.array(zbior_punktow, dtype=float)
    clusterer = DBSCAN(eps, min_samples=min_probek)
    y_pred = clusterer.fit_predict(X_clus)

    red = y_pred == 0
    blue = y_pred == 1
    cyan = y_pred == 2

    wykres = plt.figure(figsize=(10, 10))
    os = wykres.add_subplot(projection='3d')

    os.scatter(X_clus[red, 0], X_clus[red, 1], X_clus[red, 2], c="red")
    os.scatter(X_clus[blue, 0], X_clus[blue, 1], X_clus[blue, 2], c="blue")
    os.scatter(X_clus[cyan, 0], X_clus[cyan, 1], X_clus[cyan, 2], c="cyan")
    plt.title('Algorytm DBSCAN', fontsize=12)
    os.set_xlabel('x', fontsize=12)
    os.set_ylabel('y', fontsize=12)
    os.set_zlabel('z', fontsize=12)
    plt.tight_layout()
    plt.show()

Alg_DBSCAN(chmura_punktow, 50, 1000)

