from scipy.stats import norm
from csv import writer


def gen_pkt_poz(num_points:int=10000):
    os_x = norm(loc=0, scale=50)
    os_y = norm(loc=0, scale=100)
    os_z = norm(loc=-200, scale=0)

    x = os_x.rvs(size=num_points)
    y = os_y.rvs(size=num_points)
    z = os_z.rvs(size=num_points)

    points_poz = zip(x, y, z)
    return points_poz

def gen_pkt_pion(num_points:int=10000):
    os_x = norm(loc=0, scale=0)
    os_y = norm(loc=0, scale=100)
    os_z = norm(loc=0, scale=50)

    x = os_x.rvs(size=num_points)
    y = os_y.rvs(size=num_points)
    z = os_z.rvs(size=num_points)

    points_pion = zip(x, y, z)
    return points_pion

def gen_pkt_cyl(num_points:int=20000):
    os_x = norm(loc=-200, scale=50)
    os_y = norm(loc=-200, scale=50)
    os_z = norm(loc=0, scale=100)

    x = os_x.rvs(size=num_points)
    y = os_y.rvs(size=num_points)
    z = os_z.rvs(size=num_points)

    points_cyl = zip(x, y, z)
    return points_cyl

if __name__ == '__main__':
    cloud_poz = gen_pkt_poz(10000)
    with open('poziomo.xyz', 'w', encoding='utf8', newline='') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_poz:
            csvwriter.writerow(p)

    cloud_pion = gen_pkt_pion(10000)
    with open('pionowo.xyz', 'w', encoding='utf8', newline='') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_pion:
            csvwriter.writerow(p)

    cloud_cyl = gen_pkt_cyl(20000)
    with open('cylinder.xyz', 'w', encoding='utf8', newline='') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_cyl:
            csvwriter.writerow(p)

    # laczeenie do jednego pliku
    filenames = ['poziomo.xyz', 'pionowo.xyz', 'cylinder.xyz']
    with open('cloud_joined.xyz', 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)